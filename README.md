# Scripts, mostly dmenu scripts

## dmenu_menu.sh
Opens a dmenu prompt which has options:
  * Exit - exits the prompt with no output
  * Poweroff - shutdown
  * Suspend - puts your pc to sleep
  * Reboot - reboots
## get_weather.sh
Curls wttr.in for a city you specify in the scripts
  * You have to edit the variable inside the script to get accurate data.
