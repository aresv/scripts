#!/bin/bash
set -xe
setfont ter-v22b
mkfs.ext4 /dev/vda3
mkfs.ext4 /dev/vda2
mkfs.fat -F32 /dev/vda1

mount /dev/vda2 /mnt

mkdir /mnt/boot
mkdir /mnt/home

mount /dev/vda3 /mnt/home
mount /dev/vda1 /mnt/boot

pacstrap /mnt base base-devel linux-lts neovim
genfstab -U /mnt >> /mnt/etc/fstab

# Second part
echo '
#!/bin/bash
echo "void" > /etc/hostname
echo "en_US.UTF-8  UTF-8" > /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "127.0.0.1    localhost" > /etc/hosts
echo "::1          localhost" >> /etc/hosts
echo "127.0.1.1    void.localdomain void" >> /etc/hosts
sudo pacman -S networkmanager grub efibootmgr xclip xsel python-pynvim git --needed --noconfirm
systemctl enable NetworkManager.service
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg
clear
timedatectl set-ntp true
ln -sf /usr/share/zoneinfo/Europe/Belgrade /etc/localtime

useradd -mG wheel ares

sed -i "86i\%wheel ALL=(ALL) NOPASSWD: ALL" /etc/sudoers

passwd ares
' > second_part.sh
chmod +x second_part.sh
mv second_part.sh /mnt/script.sh
arch-chroot /mnt /script.sh
